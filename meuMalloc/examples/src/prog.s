# Tem q compilar na mão... não consegui "linkar" com a biblioteca
# as -g prog.s -o prog.o
# ld -I /lib/x86_64-linux-gnu/ld-linux-x86-64.so.2 -lc ../../build/*.o prog.o -o prog



# usa uma funçãozinha pra mostrar as informações básicas dos blocos (provisório!!)

.section .text
.globl _start

_start:
	pushq %rbp
	movq %rsp, %rbp

	call iniciaAlocador

	movq $1000, %rdi
	call meuMalloc

	pushq %rax

	movq $1000, %rdi
	call meuMalloc

	movq $4000, %rdi
	call meuMalloc

	popq %rdi
	call meuFree

	movq $5000, %rdi
	call meuMalloc

	movq $250, %rdi
	call meuMalloc

	movq %rax, %rdi
	call meuFree

	movq $10000, %rdi
	call meuMalloc

	movq %rax, %rdi
	call meuFree

	movq $15000, %rdi
	call meuMalloc

	call mapa

	call finalizaAlocador

	popq %rbp
	movq $0, %rdi
	movq $SYS_EXIT, %rax
	syscall
