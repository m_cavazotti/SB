// COMPILAR COM:
//gcc teste.c -g -lmeuMalloc -L ../../lib/ -o teste

//teste em C

#include <stdio.h>
#include "../../include/meuMalloc.h"

int main(){
	iniciaAlocador();
	int *a = (int *)meuMalloc(50*sizeof(int));
	int i;
	for (i = 0; i < 50; i++)
		a[i] = 2*i;
	for (i = 0; i< 50; i++)
		printf("%d\n",a[i]);

	meuFree(a);
	finalizaAlocador();
	return 0;
}
