// wget www.inf.ufpr.br/bmuller/CI064/teste.c . 


#include <stdio.h>
#include "meuMalloc.h"

int main () {
	void *a,*b,*c,*d;

	iniciaAlocador();

	a=( void * ) meuMalloc(100);
	 imprimeMapa();
	b=( void * ) meuMalloc(200);
	 imprimeMapa();
	c=( void * ) meuMalloc(300);
	 imprimeMapa();
	d=( void * ) meuMalloc(400);
	 imprimeMapa();
	meuFree(b);
	 imprimeMapa(); 
	
	b=( void * ) meuMalloc(50);
	 imprimeMapa();
	
	meuFree(c);
	 imprimeMapa(); 
	meuFree(a);
	 imprimeMapa();
	meuFree(b);
	 imprimeMapa();
	meuFree(d);
	 imprimeMapa();
	
	finalizaAlocador();
}
