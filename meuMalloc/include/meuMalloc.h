#ifndef _MEU_MALLOC_
#define _MEU_MALLOC_

extern void * meuMalloc(int);
extern int meuFree(void *);
extern void imprimeMapa();
extern void imprimeMapa_alt();
extern void iniciaAlocador();
extern void finalizaAlocador();

#endif
