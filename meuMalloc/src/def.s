.section .data

.globl fr_lst
.globl occ_list
.globl heap_begin
.globl current_break

# ######GLOBAL VARIABLES########
# This points to the beginning of the memory we are managing

# Free memory blocks list
fr_lst: .quad 0
# Occupied memory blocks list
occ_list: .quad 0
# This points to the beginning of the memory we are managing
heap_begin : .quad 0
# This points to one location past the memory we are managing
current_break : .quad 0

.globl HDR_SIZE_OFFSET
.globl HDR_AVAIL_OFFSET
.globl HDR_NEXT_OFFSET
.globl HEADER_SIZE


# #####STRUCTURE INFORMATION####

.equ HDR_SIZE_OFFSET, -8
.equ HDR_AVAIL_OFFSET, -16
.equ HDR_NEXT_OFFSET, -24
.equ HEADER_SIZE, 24

# ##########CONSTANTS###########


.globl UNAVAILABLE
.globl AVAILABLE

.globl SYS_BRK
.globl SYS_EXIT

.equ UNAVAILABLE, 1
.equ AVAILABLE, 0

.equ SYS_BRK, 12
.equ SYS_EXIT, 60

.section .text
.globl error
.globl error32

# Error: returns 0
error:
    movq $0, %rax
    popq %rbp
    ret

# Error: returns 0
# (for functions with 32 bytes of local variables)
error32:
	addq $32, %rsp
    movq $0, %rax
    popq %rbp
    ret
