.section .data

str1: .string "#"
str2: .string "+"
str3: .string "-"
str4: .string "\n"

.section .text
.globl imprimeMapa_alt

# #imprimeMapa_alt##
# Linguição


imprimeMapa_alt:
	pushq %rbp
	movq %rsp, %rbp

	movq heap_begin, %rbx # %rbx <- current search location
	addq $HEADER_SIZE, %rbx

while_heap: # scroll through the heap
	cmpq current_break, %rbx # if you have reached the end of the heap
    jg imprimeMapa_fim # end function imprimeMapa

	push %rbx
	call print_1
	popq %rbx

	# block.size / 24
	# %rax <- ratio
	# %rdx <- remainder
	movq $0, %rdx
	movq HDR_SIZE_OFFSET(%rbx), %rax
	movq $24, %r8
	divq %r8


	# ramainder > 0
	cmpq $0, %rdx
	jle for
	# print the remainder
if1:
	movq HDR_AVAIL_OFFSET(%rbx), %r8
	cmpq $AVAILABLE, %r8
	jne else1

	pushq %rbx
	pushq %rax
	call print_3
	popq %rax
	popq %rbx
	jmp for
else1:

	pushq %rbx
	pushq %rax
	call print_2
	popq %rax
	popq %rbx

for:
	# print every 24 bytes of the memory
	cmpq $0, %rax
	jle end_for
if2:
	movq HDR_AVAIL_OFFSET(%rbx), %r8
	cmpq $AVAILABLE, %r8
	jne else2

	pushq %rbx
	pushq %rax
	call print_3
	popq %rax
	popq %rbx

	jmp end_if2

else2:
	pushq %rbx
	pushq %rax
	call print_2
	popq %rax
	popq %rbx

end_if2:
	subq $1, %rax
	jmp for
end_for:

	movq HDR_SIZE_OFFSET(%rbx), %rcx
	addq $HEADER_SIZE, %rcx
	addq %rbx, %rcx # address of next block
	movq %rcx, %rbx
	jmp while_heap

imprimeMapa_fim:
    call print_4
    popq %rbp
    ret


print_1:
	movq $str1, %rax
	movq %rax, %rdi
	xor %rax, %rax
	call printf

	ret

print_2:
	movq $str2, %rax
	movq %rax, %rdi
	xor %rax, %rax
	call printf

	ret

print_3:
	movq $str3, %rax
	movq %rax, %rdi
	xor %rax, %rax
	call printf

	ret

print_4:
	movq $str4, %rax
	movq %rax, %rdi
	xor %rax, %rax
	call printf

	ret
